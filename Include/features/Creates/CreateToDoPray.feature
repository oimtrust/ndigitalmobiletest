@Feature
Feature: Create To Do Pray
  As a guest
	I can create To-Do  with title and description

  @Scenario
  Scenario Outline: Create To-Do with content about Pray
    Given I tap button with plus icon for pray
		When I type <title> and <description> for pray
		Then I tap save button for pray

    Examples: 
      | title  | description   |
      | Pray   | Good For You  |