@Feature
Feature: Create To Do Sleep
  As a guest
	I can create To-Do  with title and description

  @Scenario
  Scenario Outline: Create To-Do with content about Sleep
    Given I open the ToDoApp
		When I tap button with plus icon
		And I enter title <title> and description <description>
		Then I tap save button

    Examples: 
      | title  | description   |
      | Sleep  | Sleep is good |