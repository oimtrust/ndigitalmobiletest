@Feature
Feature: Create To Do Eat
  As a guest
	I can create To-Do  with title and description

  @Scenario
  Scenario Outline: Create To-Do with content about Eat
    Given I tap button with plus icon for eat
		When I enter <title> and <description> for eat
		Then I tap save button for eat

    Examples: 
      | title  | description   |
      | Eat    | Eat for life  |