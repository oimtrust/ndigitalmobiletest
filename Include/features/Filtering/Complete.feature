@Feature
Feature: Filter To Do
  I want to filter To Do for "Complete" only

  @Scenario
  Scenario: Filter To Do for "Complete" only
    Given I tap filter button
    When I choose Complete check
    Then I see list of complete todos